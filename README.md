<h1>PUB for MAC</h1>

[![68747470733a2f2f61736369696e656d612e6f72672f612f3131323035352e706e67.png](https://bitbucket.org/repo/gdbdag/images/17258258-68747470733a2f2f61736369696e656d612e6f72672f612f3131323035352e706e67.png)](https://asciinema.org/a/112055)

# **Mobile assets factory command-line program **

PUB is a command-line program to create iOS and Android app icons assets.

Just run pub on the folder with the default icon image(.png 1536x1536) to create all app icons.

Ex: 
~myfolder: pub ios

Run on terminal:

~ pub
  
  or
  
~ pub ios
  
  or
  
~ pub android


### Dependencies ###

**It requires the curl, imagemagick and jquery installed.** 
# Install #

To install it right away for MAC users, copy and paste on terminal:


```
#!shell

sudo wget https://raw.githubusercontent.com/sevenb/pub/master/pub -O /usr/local/bin/pub
sudo chmod a+rx /usr/local/bin/pub
```

or 


```
#!shell

sudo curl -L https://raw.githubusercontent.com/sevenb/pub/master/pub -o /usr/local/bin/pub
sudo chmod a+rx /usr/local/bin/pub
```